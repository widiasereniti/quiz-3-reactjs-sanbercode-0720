import React from "react";

let dataDiri = [{
    nama: "Widia Sereniti", 
    email: "widiasereniti@gmail.com", 
    so: "windows",
    gitlab: "https://gitlab.com/widiasereniti/sanbercode-reactjs-0720",
    accounttg: "087781398934"
}]

class AboutData extends React.Component {
    render() {
        return (
            <>   
                <ol>
                    <li><strong>Nama</strong>: {this.props.profile.nama}</li>
                    <li><strong>Email</strong>: {this.props.profile.email}</li>
                    <li><strong>Sistem Operasi yang digunakan</strong>: {this.props.profile.so}</li>
                    <li><strong>Akun Gitlab</strong>: {this.props.profile.gitlab}</li>
                    <li><strong>Akun Telegram</strong>: {this.props.profile.accounttg}</li>
                </ol>
            </>
        )
    }
}

function About() {
    return (
        <>
            <section>
                <div>
                    <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
                    {dataDiri.map(data => {
                        return (
                            <AboutData profile={data} />
                        )
                    })}
                </div>
            </section>
        </>
    )
}

export default About
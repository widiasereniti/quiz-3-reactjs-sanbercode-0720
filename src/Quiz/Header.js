import React, {useContext} from 'react';
import {Link} from 'react-router-dom';
import logo from '../Public/img/logo.png';
import '../Public/css/style.css'
import {LoginContext} from './Login/loginContext';

function Header() {
    const [login] = useContext(LoginContext)

    return (
        <>
            <header>
                <img id="logo" src={logo} alt="logo" width="200px"/>
                <nav>
                    <ul>
                        <li>
                            <Link to="/home">Home</Link>
                        </li>
                        <li>
                            <Link to="/about">About</Link>
                        </li>
                        {login === true &&
                            <li>
                                <Link to="/movie">Movie List Editor</Link>
                            </li>
                        }
                        {login === true ?
                            <li>
                                <Link to="/login">Logout</Link>
                            </li>
                            :
                            <li>
                                <Link to="/login">Login</Link>
                            </li>
                        }
                    </ul>
                </nav>
            </header>
        </>
    )
}

export default Header
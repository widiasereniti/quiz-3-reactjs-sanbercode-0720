import React, {useEffect, useState} from 'react';
import axios from 'axios'

// import {LoginContext} from "./Login/LoginContext";

const Movie = () => {
    const [dataMovie, setDataMovie] = useState(null)
    const [inputTitle, setInputTitle] = useState("")
    const [inputDescription, setInputDescription] = useState("")
    const [inputYear, setInputYear] = useState("")
    const [inputDuration, setInputDuration] = useState("")
    const [inputGenre, setInputGenre] = useState("")
    const [inputRating, setInputRating] = useState("")
    const [selectedId, setSelectedId]  =  useState(0)
    const [statusForm, setStatusForm]  =  useState("create")

    useEffect( () => {
        if (dataMovie === null){
        axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then(res => {
                setDataMovie(res.data.map(el => { 
                    return {
                        id: el.id,
                        title: el.title,
                        description: el.description,
                        year: el.year,
                        duration: el.duration,
                        genre: el.genre,
                        rating: el.rating
                    }
                }))
            })
        }
    }, [dataMovie])


    const handleDelete = (event) => {
        const { value } = event.target
        let idMovie = parseInt(value)

        let newDataMovie = dataMovie.filter(item => item.id !== idMovie)
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovie}`)
        .then(res=>{
            console.log(res);
        })

        setDataMovie([...newDataMovie])
    }

    const handleEdit = (event) =>{
        const { value } = event.target
        let idBuah = parseInt(value)
        let newBuah = dataMovie.find(x=> x.id === idBuah)

        setInputTitle(newBuah.title)
        setInputDescription(newBuah.description)
        setInputYear(newBuah.year)
        setInputDuration(newBuah.duration)
        setInputGenre(newBuah.genre)
        setInputRating(newBuah.rating)
        setSelectedId(idBuah)
        setStatusForm("edit")
    }

    const handleChange = (event) =>{
        const { name, value } = event.target
        switch(name){
            case "Title":
                setInputTitle(value);
                break;
            case "Description":
                setInputDescription(value);
                break;
            case "Year":
                setInputYear(value);
                break;
            case "Duration":
                setInputDuration(value);
                break;
            case "Genre":
                setInputGenre(value);
                break;
            case "Rating":
                setInputRating(value);
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event) =>{

        event.preventDefault()
        let title = inputTitle
        let description = inputDescription
        let year = inputYear
        let duration = inputDuration
        let genre = inputGenre
        let rating = inputRating

        if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" &&
            year.toString().replace(/\s/g,'') !== "" && duration.toString().replace(/\s/g,'') !== "" &&
            genre.replace(/\s/g,'') !== "" && rating.toString().replace(/\s/g,'') !== ""){
            if (statusForm === "create"){
                axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title, description, year, duration, genre, rating})
                .then(res => {
                    setDataMovie([...dataMovie, {
                        id: res.id,
                        title: title,
                        description: description,
                        year: year,
                        duration: duration,
                        genre: genre,
                        rating: rating
                    }])
                })
            }else if(statusForm === "edit"){
                axios.put(`http://backendexample.sanbercloud.com/api/movies/${selectedId}`, {title, description, year, duration, genre, rating})
                .then(res => {
                    let dataNewMovie = dataMovie.find(el=> el.id === selectedId)
                    dataNewMovie.title = title
                    dataNewMovie.description = description
                    dataNewMovie.year = year
                    dataNewMovie.duration = duration
                    dataNewMovie.genre = genre
                    dataNewMovie.rating = rating
                    setDataMovie([...dataMovie])
                })
            }
            
            setStatusForm("create")
            setSelectedId(0)
            setInputTitle("")
            setInputDescription("")
            setInputYear("")
            setInputDuration("")
            setInputGenre("")
            setInputRating("")
        }
    }
    
    return (
        <>
            <section >
                <h1>Daftar Film Film Terbaik</h1>
                <table style={{marginLeft: '15%'}}>
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Year</th>
                            <th>Duration</th>
                            <th>Genre</th>
                            <th>Rating</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            dataMovie !== null && dataMovie.map((data, index)=>{
                                return(                    
                                    <tr key={index}>
                                        <td>{data.title}</td>
                                        <td>{data.description}</td>
                                        <td>{data.year}</td>
                                        <td>{data.duration}</td>
                                        <td>{data.genre}</td>
                                        <td>{data.rating}</td>
                                        <td>
                                            <button onClick={handleEdit} value={data.id}>Edit</button>
                                            &nbsp;
                                            <button onClick={handleDelete} value={data.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>

                <h1>Form Film</h1>
                <form onSubmit={handleSubmit} style={{marginLeft: '35%'}}>
                    <label>Title:</label>          
                    <input 
                        type="text"
                        name="Title"
                        value={inputTitle}
                        onChange={handleChange}
                        placeholder="Title"
                    /><br/>
                    <label>Description:</label>          
                    <input 
                        type="text" 
                        name="Description"
                        value={inputDescription}
                        onChange={handleChange}
                        placeholder="Description"
                    /><br/>
                    <label>Year:</label>          
                    <input 
                        type="text"
                        name="Year"
                        value={inputYear}
                        onChange={handleChange}
                        placeholder="Year"
                    /><br/>
                    <label>Duration:</label>          
                    <input 
                        type="text"
                        name="Duration"
                        value={inputDuration}
                        onChange={handleChange}
                        placeholder="Duration"
                    /><br/>
                    <label>Genre:</label>          
                    <input 
                        type="text"
                        name="Genre"
                        value={inputGenre}
                        onChange={handleChange}
                        placeholder="Genre"
                    /><br/>
                    <label>Rating:</label>          
                    <input 
                        type="number"
                        min="1"
                        max="10"
                        name="Rating"
                        value={inputRating}
                        onChange={handleChange}
                        placeholder="Rating"
                    /><br/>
                    <button>submit</button>
                </form>
            </section>
        </>
    )
}

export default Movie
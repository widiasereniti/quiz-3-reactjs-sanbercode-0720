import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Header from './Header';
import Footer from './Footer'
import Home from './Home/index';
import About from './About/index';
import Movie from './Movie/index';
import Login from './Login/index';
import {LoginProvider} from './Login/loginContext';

const Routes = () => {

    return (
        <>
            <LoginProvider>
                <Header />
                <Switch>
                    <Route path="/home">
                        <Home />
                    </Route>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/movie">
                        <Movie />
                    </Route>
                    <Route path="/login">
                        <Login/>
                    </Route>
                </Switch>
                <Footer />
            </LoginProvider>
        </>
    );
};

export default Routes
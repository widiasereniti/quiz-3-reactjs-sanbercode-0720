import React, {useContext} from "react";
import {LoginContext, LoginProvider} from "./loginContext";
import '../../Public/css/style.css'

function Login() {
    const [login, setLogin] = useContext(LoginContext)

    const handleClick = (e) =>{
        e.preventDefault();
        setLogin(login === false ? true : false)
    }

    return(
        <LoginProvider>
        <section>
            <form>
                <h1>Klik tombol ini untuk login/logout</h1>
                <button onClick={handleClick}>Submit</button>
                <br/>
            </form>
        </section>
        </LoginProvider>
    )
}

export default Login
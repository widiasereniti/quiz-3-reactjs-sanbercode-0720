import React, {Component} from 'react';
import axios from 'axios'

class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            dataMovie: []
        }
    }

    componentDidMount(){
        // read array data
        if (this.state.dataMovie.length === 0) {
            axios.get(`http://backendexample.sanbercloud.com/api/movies`)
            .then((response) => {
                const dataMovie = response.data;
                this.setState({ dataMovie });
            });
        }
    }

    render(){
        const { dataMovie } = this.state
        // console.log(dataMovie)

        //sorting
        let movieList = dataMovie.slice(0);
        movieList.sort((a, b) => (a.rating < b.rating ? 1 : -1));

        return(
            <section>
                <h1>Daftar Film Film Terbaik</h1>
                <div>
                    {
                        movieList.map((data, index) => {
                            return(
                                <div key={index}>
                                    <h1 style={{textAlign:'left', color:'purple'}}><strong>{data.title}</strong></h1>
                                    <ul>
                                        <li style={{listStyleType: 'none'}}><strong>Rating: {data.rating}</strong></li>
                                        <li style={{listStyleType: 'none'}}><strong>Durasi: {Math.ceil(data.duration/60)} jam</strong></li>
                                        <li style={{listStyleType: 'none'}}><strong>Genre: {data.genre}</strong></li>
                                        <li style={{listStyleType: 'none'}}><strong>Deskripsi:</strong> {data.description}</li>
                                    </ul> 
                                </div>
                            )
                        })
                    }
                </div>
            </section>
        )
    }
}

export default Home